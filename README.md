# The Whisky Explorer - SMWS Codes Lookup

A code lookup tool for SMWS built in React.

## 🔗 Links

[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/mgilberties)
